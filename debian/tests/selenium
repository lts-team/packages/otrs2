#!/bin/sh

# Cf. https://github.com/znuny/Znuny/blob/dev/.github/workflows/ci.yaml

set -eu
PATH="/usr/sbin:/sbin:/usr/bin:/bin"
export PATH

BASEDIR="$(pwd)"

cd /usr/share/otrs
install -o otrs -m0700 -d /var/lib/otrs/.gnupg
ln -sT /var/lib/otrs/.gnupg ~otrs/.gnupg

runuser -u otrs -- bin/otrs.CheckModules.pl
runuser -u otrs -- perl -cw bin/cgi-bin/index.pl
runuser -u otrs -- bin/otrs.Console.pl Maint::Database::Check

SELENIUM_VERSION="3.141.59"
install -o "$AUTOPKGTEST_NORMAL_USER" -g root -m0755 -d "$AUTOPKGTEST_TMP/extern"
runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
    curl -Lfso "$AUTOPKGTEST_TMP/extern/selenium-server-standalone-$SELENIUM_VERSION.jar" \
         "https://github.com/SeleniumHQ/selenium/releases/download/selenium-$SELENIUM_VERSION/selenium-server-standalone-$SELENIUM_VERSION.jar"

CHROMEDRIVER_VERSION="90.0.4430.24" # buster has chromium 90
runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
    curl -Lfso "$AUTOPKGTEST_TMP/extern/chromedriver_linux64.zip" \
         "https://chromedriver.storage.googleapis.com/$CHROMEDRIVER_VERSION/chromedriver_linux64.zip"
runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
    unzip "$AUTOPKGTEST_TMP/extern/chromedriver_linux64.zip" \
        chromedriver \
        -d "$AUTOPKGTEST_TMP/extern"

runuser -u "$AUTOPKGTEST_NORMAL_USER" -- \
    java -Dwebdriver.chrome.driver="$AUTOPKGTEST_TMP/extern/chromedriver" \
         -jar "$AUTOPKGTEST_TMP/extern/selenium-server-standalone-$SELENIUM_VERSION.jar" \
         -port 4444 -timeout 320 -browserTimeout 320 >"$AUTOPKGTEST_TMP/selenium.log" 2>&1 &
sleep 2

runuser -u postgres -- psql <<-EOF
	CREATE ROLE otrstest LOGIN NOSUPERUSER NOCREATEDB NOCREATEROLE;
	CREATE DATABASE otrstest OWNER otrstest ENCODING 'UTF8';
	ALTER ROLE otrstest PASSWORD 'otrstest';
EOF

# Cf. https://github.com/znuny/Znuny/blob/dev/.github/workflows/ci/config-selenium.sh
install -ootrs -gwww-data -m0640 Kernel/Config.pm "$BASEDIR/Kernel/Config.pm"
sed -ri 's|^\s+return\s*[0-9]*;|    $Self->{"TestHTTPHostname"} = "127.0.1.1";\
    $Self->{TestDatabase}     = {                                             \
        DatabaseDSN  => "DBI:Pg:dbname=otrstest;host=$Self->{DatabaseHost}",  \
        DatabaseUser => "otrstest",                                           \
        DatabasePw   => "otrstest"                                            \
    };                                                                        \
    $Self->{"SeleniumTestsConfig"} = {                                        \
       remote_server_addr  => "127.0.0.1",                                    \
       port                => "4444",                                         \
       platform            => "ANY",                                          \
       browser_name        => "chrome",                                       \
       extra_capabilities => {                                                \
           chromeOptions => {                                                 \
               # disable-infobars makes sure window size calculations are ok  \
               args => [                                                      \
                   "headless",                                                \
                   "no-sandbox",                                              \
                   "disable-infobars"                                         \
               ]                                                              \
           }                                                                  \
       }                                                                      \
    };                                                                        \
    $Self->{"UnitTest::Blacklist"} = {                                        \
        foo => [                                                              \
            "Agent/Admin/Installer.t",                                        \
            "Customer/Ajax/ErrorHandling.t",                                  \
            "Output/Dashboard/ProductNotify.t"                                \
        ]                                                                     \
    };\n&|' "$BASEDIR/Kernel/Config.pm"

cd "$BASEDIR"
install -o otrs -g www-data -m0750 -Dd "$AUTOPKGTEST_TMP/SeleniumScreenshots"
ln -sT "$AUTOPKGTEST_TMP/SeleniumScreenshots" var/httpd/htdocs/SeleniumScreenshots
ln -sT "$AUTOPKGTEST_TMP/SeleniumScreenshots" /var/lib/otrs/httpd/htdocs/SeleniumScreenshots

rm -rf Kernel/Config/Files
ln -sT /var/lib/otrs/Config/Files Kernel/Config/Files
runuser -u otrs -- bin/otrs.Console.pl Dev::UnitTest::Run --directory Selenium
